CREATE DATABASE database_nomina;

USE database_nomina;

CREATE TABLE employees(
    id INT(11) NOT NULL,
    first_name VARCHAR(16) NOT NULL,
    last_name VARCHAR (16) NOT NULL,
    id_card VARCHAR (25) NOT NULL,
    address VARCHAR(100) NOT NULL,
    phone VARCHAR (15) NOT NULL,
    email VARCHAR(25) NOT NULL,
    entry_date DATE NULL,
    egress_date DATE NULL
);

ALTER TABLE employees   
    ADD PRIMARY KEY (id);

ALTER TABLE employees   
    MODIFY id INT(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT = 2;

DESCRIBE employees;