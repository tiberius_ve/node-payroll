const express = require('express');
const router = express.Router();

const pool = require('../database');

router.get('/add', (req, res) => {
    res.render('home/add');
})

router.post('/add', async (req, res) => {
    const { id, first_name, last_name, id_card, address, phone, email, entry_date, egress_date } = req.body;
    const newEmployee = {
        id,
        first_name,
        last_name,
        id_card,
        address,
        phone,
        email,
        entry_date,
        egress_date
    }
    await pool.query('INSERT INTO employees set ?', [newEmployee]);
    res.redirect('/home');
})

router.get('/', async (req, res) => {
    const employees = await pool.query('SELECT * FROM employees');
    console.log(employees);
    res.render('home/home', { employees });
})

module.exports = router;